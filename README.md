# ReelJS

ReelJS is a responsive carousel.

### Features

* Responsive design
* Lazy loading images
* CSS3 animation
* Touch events support  
