/**
 * Reel.js
 *
 * Walter R.
 * 2012
 */
;(function(global, doc) {
    
    var reel,

        // toString shortcut
        toString = Object.prototype.toString,

        // dummy style
        dummyStyle = doc.createElement('div').style,

        // Default settings
        configuration = {
            
            // @cfg int index Slide to start
            index    : 0,

            // @cfg int speed
            speed    : 300,

            // @cfg callback function
            callback : function(){},

            // @cfg int bufferSize
            bufferSize: 7,

            // @cfg int limitLoad
            limitLoad: 3,

            items: []

        },

        // Get vendor
        vendor = (function() {
            var vendors = 't,webkitT,MozT,msT,OT'.split(','),
                t,
                i = 0,
                len = vendors.length;

                for( ; i < len; i++) {
                    t = vendors[i] + 'ransform';
                    
                    if(t in dummyStyle) {
                        return vendors[i].substr(0, vendors[i].length - 1);
                    }
                }
        }()),

        queue = [],

        /**
         * Returns prefix style -webkit-..., -moz-... -o-...
         */
        prefixStyle = function(style) {
            if(vendor === '') return style;

            style = style.charAt(0).toUpperCase() + style.substr(1);

            return vendor + style;
        },

        cssVendor = vendor ? '-' + vendor.toLowerCase() + '-' : '',

        // Browser capabilities
        has3D               = prefixStyle('perspective') in dummyStyle,
        hasTouch            = 'ontouchstart' in global,
        hasTransitionEnd    = prefixStyle('transition') in dummyStyle,

        // Translate style properties
        transform           = prefixStyle('transform'),
        transitionDuration  = prefixStyle('transitionDuration'),
        translateZ          = has3D ? ' translateZ(0)' : '',

        // Events
        resizeEvent         = 'onorientationchange' in global ? 'orientationchange' : 'resize',
        startEvent          = hasTouch ? 'touchstart' : 'mousedown',
        moveEvent           = hasTouch ? 'touchmove' : 'mousemove',
        endEvent            = hasTouch ? 'touchend' : 'mouseup',
        cancelEvent         = hasTouch ? 'touchcancel' : 'mouseup',
        transitionEndEvent  = (function() {
            if(vendor === false) return false;

            var transitionEnd = {
                ''          : 'transitionend',
                'webkit'    : 'webkitTransitionEnd',
                'Moz'       : 'transitionend',
                'O'         : 'oTransitionEnd',
                'ms'        : 'MSTransitionEnd'
            };

            return transitionEnd[vendor];
        }());


        /**
         * Constructor
         */
        reel = function(element, options) {
            var i,
                slider,
                fragment = doc.createDocumentFragment(),
                items;

            this.wrapper = typeof element === 'string' ? doc.querySelector(element) : el;

            // master pages
            this.masterPages = [];

            // merge configuration
            for(var i in options) {
                configuration[i] = options[i];
            }

            // index
            this.index = configuration.index;

            // items
            this.items = items = configuration.items;

            // wrapper styles
            this.wrapper.cssText = 'overflow: hidden; position: relative';

            // Reel slider
            this.slider = slider = doc.createElement('ul');
            slider.id   = 'slider';

            // listeners
            global.addEventListener(resizeEvent, this, false);
            global.addEventListener(transitionEndEvent, this, false);

            // initial structure
            for(var i = 0; i < configuration.limitLoad; i++) {
                panel               = doc.createElement('li');
                panel.dataset.index = i;
                panel.dataset.type  = 'load';
                panel.innerHTML     = items[i].html;

                slider.appendChild(panel);

                this.masterPages[i] = {
                    index : i,
                    item  : panel,
                    type  : 'load'
                };
            }

            fragment.appendChild(slider);
            
            this.wrapper.appendChild(fragment);

            // refresh dimensions
            this.refreshSize();

            // setup
            this.setup();
        }

        // Reel prototype
        reel.prototype = {

            wrapperWidth: 0,

            sliderWidth: 0,

            // unload | unload | loaded | visible | loaded | unload
            setup: function() {
                

                // Set slider width
                //slider.style.width = (children)
            },

            /**
             * Refresh Reel dimensions
             */
            refreshSize: function() {
                var wrapper      = this.wrapper,
                    wrapperWidth = wrapper.getBoundingClientRect().width || wrapper.offsetWidth,
                    slider       = this.slider,
                    masterPagesSize,
                    pages = this.masterPages,
                    p,
                    children = slider.children;

                if(!wrapperWidth) {
                    throw new Error("Can't calculate current wrapper and container width");
                }

                this.wrapperWidth    = wrapperWidth;
                masterPagesSize      = this.getMasterPagesSize();
                this.sliderWidth     = wrapperWidth * masterPagesSize;

                this.slider.style.width = this.sliderWidth + 'px';

                for(var i = 0, len = children.length; i < len; i++) {
                    p = pages[children[i].dataset.index];

                    if(p.type === 'dummy') {
                        children[i].style.width = (p.count * wrapperWidth) + 'px';
                    }
                    else {
                        children[i].style.width = wrapperWidth + 'px';
                    }
                }
            },

            /**
             * Refresh slider width
             */
            refreshSliderWidth: function() {
                this.slider.style.width = (this.wrapperWidth * this.getMasterPagesSize()) + 'px';
            },

            /**
             * Returns how many items are in the reel
             */
            getMasterPagesSize: function() {
                var size = 0,
                    pages = this.masterPages, 
                    p;

                for(var i = 0, len = pages.length; i < len; i++) {
                    p = pages[i];

                    if(p.type === 'dummy') {
                        size = size + p.count;
                    }
                    else if(p.type === 'load' || p.type === 'unload') {
                        size = size + 1;
                    }
                }

                return size;
            },

            /**
             *
             */
            next: function() {
                var to = this.index + 1;

                if(this.masterPages[to]) {
                    this.goToPage(to);

                    this.index = to;
                }
                else {
                    if(this.index < this.items.length-1) {
                        this.sortReel('next');
                        this.next();
                    }
                    else {
                        console.log('no more to load');
                    }
                }
            },

            /**
             *
             */
            prev: function() {
                var to = this.index - 1;

                if(this.masterPages[to]) {
                    this.goToPage(to);

                    this.index = to;
                }
                else {
                    console.log('No more prev items');
                }
            },

            /**
             *
             */
            sortReel: function(dir) {
                var items     = this.items,
                    item,
                    loadItems = [],
                    fragment  = doc.createDocumentFragment(),
                    panel,
                    needRefresh = false,
                    masterLen,
                    from = this.index + 1,
                    to   = from + configuration.limitLoad;

                // next
                if(dir === 'next') {
                    for(var i = from; i < to; i ++ ) {
                        item = items[i];

                        if(item) {
                            loadItems.push(item);
                        }
                    }

                    if(loadItems) {
                        for(var i = 0, len = loadItems.length; i < len; i++ ) {
                            masterLen           = this.masterPages.length;
                            panel               = doc.createElement('li');
                            panel.className     = 'unload';
                            panel.dataset.index = masterLen;
                            panel.dataset.type  = 'unload';
                            panel.innerHTML     = loadItems[i].html;
                            panel.style.width   = this.wrapperWidth + 'px';

                            fragment.appendChild(panel);

                            this.masterPages[masterLen] = {
                                index : masterLen,
                                item  : panel,
                                type  : 'load'
                            };
                        }

                        // @TODO refresh just slider width
                        this.refreshSliderWidth();

                        queue.push(function() {
                            // @TODO on transition end then load images
                            slider.appendChild(fragment);
                        });
                        
                        

                        needRefresh = true;
                    }

                    console.log('Load more items: ', loadItems);
                }

                // prev
                if(dir === 'prev') {

                }

                if(needRefresh) {
                    //this.refreshSize();

                    return true;
                }
                else {
                    return false;
                }
            },

            /**
             *
             */
            goToPage: function(p) {
                var x = p * this.wrapperWidth;
                console.log('go to page: ', p, p * this.wrapperWidth);

                this.translate(x);
            },

            onResize: function() {
                this.refreshSize();
            },

            /**
             * Translate CSS3 function
             * This function translates an Element in X
             *
             * @private
             */
            translate: function(elm, x, speed) {
                var style;

                if(typeof elm === 'number') {
                    x   = elm;
                    elm = this.slider;
                }

                if(!elm) {
                    throw new Error("Invalid element to be translate");
                }

                x = (Math.abs(x))*-1;

                style = elm.style;

                speed = speed >= 0 ? speed : configuration.speed;

                console.log('Translating element: ', speed);

                style.webkitTransitionDuration = 
                style.MozTransitionDuration = 
                style.msTransitionDuration = 
                style.OTransitionDuration = 
                style.transitionDuration = speed + 'ms';

                // translate to given position
                style.webkitTransform = 'translate(' + x + 'px,0)' + 'translateZ(0)';
                style.msTransform = 
                style.MozTransform = 
                style.OTransform = 'translateX(' + x + 'px)';
            },

            /**
             *
             */
            onTransitionEnd: function() {
                var fn;

                while( (fn = queue.shift()) ) {
                    fn.call(this);
                }
            },

            /**
             * Event handler function
             */
            handleEvent: function(evt) {
                switch(evt.type) {
                    // resize
                    case 'resize': this.onResize(); break;
                    case transitionEndEvent: this.onTransitionEnd(); break;
                }
            }

        };

        global.Reel = reel;

}(window, document));