var Elements = [
    {
        html: [
            '<h1>1</h1>',
            '<div class="box portrait">',
                '<span class="portrait small">',
                    '<img src="img/1/small.jpg" border="0" alt="" />',
                '</span>',

                '<span class="portrait large">',
                    '<img src="img/1/large.jpg" border="0" alt="" />',
                '</span>',

                '<span class="portrait medium">',
                    '<img src="img/1/medium.jpg" border="0" alt="" />',
                '</span>',
            '</div>'
        ].join(" ")
    },

    {
        html: [
            '<h1>2</h1>',
            '<div class="box landscape">',

                '<span class="landscape small">',
                    '<img src="img/2/small.jpg" border="0" alt="" />',
                '</span>',

                '<span class="landscape large">',
                    '<img src="img/2/large.jpg" border="0" alt="" />',
                '</span>',

                '<span class="landscape medium">',
                    '<img src="img/2/medium.jpg" border="0" alt="" />',
                '</span>',
            '</div>'
        ].join(" ")
    },

    {
        html: [
            '<h1>3</h1>',
            '<div class="box landscape">',

                '<span class="landscape small">',
                    '<img src="img/3/small.jpg" border="0" alt="" />',
                '</span>',

                '<span class="landscape large">',
                    '<img src="img/3/large.jpg" border="0" alt="" />',
                '</span>',

                '<span class="landscape medium">',
                    '<img src="img/3/medium.jpg" border="0" alt="" />',
                '</span>',
            '</div>'
        ].join(" ")
    },

    {
        html: [
            '<h1>4</h1>',
            '<div class="box landscape">',

                '<span class="landscape small">',
                    '<img src="img/4/small.jpg" border="0" alt="" />',
                '</span>',

                '<span class="landscape large">',
                    '<img src="img/4/large.jpg" border="0" alt="" />',
                '</span>',

                '<span class="landscape medium">',
                    '<img src="img/4/medium.jpg" border="0" alt="" />',
                '</span>',
            '</div>'
        ].join(" ")
    },

    {
        html: [
            '<h1>5</h1>',
            '<div class="box portrait">',
                '<span class="portrait small">',
                    '<img src="img/1/small.jpg" border="0" alt="" />',
                '</span>',

                '<span class="portrait large">',
                    '<img src="img/1/large.jpg" border="0" alt="" />',
                '</span>',

                '<span class="portrait medium">',
                    '<img src="img/1/medium.jpg" border="0" alt="" />',
                '</span>',
            '</div>'
        ].join(" ")
    },

    {
        html: [
            '<h1>6</h1>',
            '<div class="box landscape">',

                '<span class="landscape small">',
                    '<img src="img/2/small.jpg" border="0" alt="" />',
                '</span>',

                '<span class="landscape large">',
                    '<img src="img/2/large.jpg" border="0" alt="" />',
                '</span>',

                '<span class="landscape medium">',
                    '<img src="img/2/medium.jpg" border="0" alt="" />',
                '</span>',
            '</div>'
        ].join(" ")
    },

    {
        html: [
            '<h1>7</h1>',
            '<div class="box landscape">',

                '<span class="landscape small">',
                    '<img src="img/3/small.jpg" border="0" alt="" />',
                '</span>',

                '<span class="landscape large">',
                    '<img src="img/3/large.jpg" border="0" alt="" />',
                '</span>',

                '<span class="landscape medium">',
                    '<img src="img/3/medium.jpg" border="0" alt="" />',
                '</span>',
            '</div>'
        ].join(" ")
    },

    {
        html: [
            '<h1>8</h1>',
            '<div class="box landscape">',

                '<span class="landscape small">',
                    '<img src="img/4/small.jpg" border="0" alt="" />',
                '</span>',

                '<span class="landscape large">',
                    '<img src="img/4/large.jpg" border="0" alt="" />',
                '</span>',

                '<span class="landscape medium">',
                    '<img src="img/4/medium.jpg" border="0" alt="" />',
                '</span>',
            '</div>'
        ].join(" ")
    },

    {
        html: [
            '<h1>9</h1>',
            '<div class="box portrait">',
                '<span class="portrait small">',
                    '<img src="img/1/small.jpg" border="0" alt="" />',
                '</span>',

                '<span class="portrait large">',
                    '<img src="img/1/large.jpg" border="0" alt="" />',
                '</span>',

                '<span class="portrait medium">',
                    '<img src="img/1/medium.jpg" border="0" alt="" />',
                '</span>',
            '</div>'
        ].join(" ")
    }
];